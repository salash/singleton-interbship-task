package com.example.services;


import java.io.IOException;
import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PingService.class)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PingServiceTest {
	
	@Test
	public void checkSuccessTest() throws Exception{
		
		PingService ping=new PingService();
		boolean result = ping.checkSuccess(null, "httpURLConnection: httpURLConnection");
	//	assertThat(result).isTrue();
		assertEquals(result, false);
	}
	
	@Test
	public void editFailureNumbersTest() throws Exception{
		int startingValue=PingService.failures;
		PingService ping=new PingService();
		int failures=ping.editFailureNumbers(false);
		assertEquals(failures-startingValue, 1);
		
	}
	
	@Test
	public void shouldAlarmTest() throws Exception{
		PingService ping=new PingService();
		ping.editFailureNumbers(false);
		ping.editFailureNumbers(false);
		ping.editFailureNumbers(false);
		//ping.editFailureNumbers(false);
		System.out.println("Failures are:"+PingService.failures);
		 boolean shouldAlarm = ping.shouldAlarm();
		assertEquals(shouldAlarm, false);
		
		
		
	}
}
