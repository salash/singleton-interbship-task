package com.example.readConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import lombok.Data;

@Data

public class ReadConfig {
	
	Double request_interval;
	Double request_timeout_limit;
	String phone;

	private ArrayList alert_conditions;
	
	public  ReadConfig() throws FileNotFoundException, IOException, ParseException{
	    
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("config.txt").getFile());
		if(file.exists() && !file.isDirectory()) { 
			//   System.out.println("File Exist");
			   try {
				   Object obj = parser.parse(new FileReader(file));
				   JSONObject jsonObject = (JSONObject) obj;
				   
				    request_interval = (Double) jsonObject.get("request_interval");
		            request_timeout_limit = (Double) jsonObject.get("request_timeout_limit");
		            phone = (String) jsonObject.get("phone");
		           JSONArray alertList = (JSONArray) jsonObject.get("alert_conditions");
		           fillAlertList(alertList);
/*		           
		           System.out.println("request_interval: " + request_interval);
		           System.out.println("request_timeout_limit: " + request_timeout_limit);
		           System.out.println("phone: " + phone);
		          
		           System.out.println("\nAlarm List:");
		           Iterator<Long> iterator = alertList.iterator();
		           while (iterator.hasNext()) {
		               System.out.println("Alert is:"+iterator.next());
		          //    alert_conditions.add( iterator.next().intValue());
		           }
*/		
		    } catch (Exception e) {
		           e.printStackTrace();
		       }

		
		 }
		
			else
			  System.out.println("File NOT Exist");	
				
	}

	public ArrayList fillAlertList(JSONArray jsonArray ) {      
		if (jsonArray != null) { 
		   int len = jsonArray.size();
		   alert_conditions=new ArrayList<>(len);
		   for (int i=0;i<len;i++){ 
		    alert_conditions.add(jsonArray.get(i));
		   } 
		}
		 System.out.println("alert_conditions:  "+alert_conditions);
		return alert_conditions;
		
	}

}
