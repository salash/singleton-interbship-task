package com.example.model;


import lombok.Data;

@Data
public class Ping {
	String url;
	String expected_content;

}
