package com.example.services;




import com.example.readConfig.ReadConfig;
import com.example.readConfig.URI;

//@Service
public class SchedulerService implements Runnable {

	public static Thread t=null;
	
	@Override
	public void run() {
		 
		try {
			PingService PingService=new PingService();
			ReadConfig config = new ReadConfig();
		
			
			while(true){
				
				String URL=URI.uri;
				Double timeout=config.getRequest_timeout_limit();
				
				PingService.makePing(URL,	timeout,URI.expected_string);
			
			    Thread.sleep(config.getRequest_interval().intValue());
			
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	public  void killPingerThread() {
		try{
			
			if(SchedulerService.t!=null){
				SchedulerService.t.stop();
				SchedulerService.t=null;	
			}
		}catch(Exception e)
		{
			SchedulerService.t=null;	
			System.out.println("Previous thread is killed");
		}
		
	}

	public  void savePingerThread(Thread newURL) {
		SchedulerService.t=newURL;
		
	}

	public void makeNewPingerThread() {
		Thread newURL = new Thread(new SchedulerService());
		savePingerThread(newURL);
		
	}

	public void pingerStarter() {
		SchedulerService.t.start();
	}

	public void scheduleManager() {
		killPingerThread();
		makeNewPingerThread();
		pingerStarter();
		
	}
	
}
