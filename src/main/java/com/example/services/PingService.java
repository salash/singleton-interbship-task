package com.example.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.example.readConfig.ReadConfig;

import lombok.Data;


public class PingService {
	
	
	public static int failures=0;
	
	@SuppressWarnings("deprecation")
	public HttpURLConnection GetRequest(String urlToPing, Double timeout) throws URISyntaxException, ClientProtocolException, IOException{

		try {
			System.out.println("URL is:   "+urlToPing);
			   HttpURLConnection.setFollowRedirects(false);
			   HttpURLConnection con = (HttpURLConnection) new URL(urlToPing).openConnection();
			   con.setRequestMethod("HEAD");

			   con.setConnectTimeout( timeout.intValue());  
			   
               System.out.println("########################### httpURLConnection: "+con.getResponseCode());
               return con;
			//   return (con.getResponseCode() == HttpURLConnection.HTTP_OK );
			} catch (java.net.SocketTimeoutException e) {
			   return null;
			} catch (java.io.IOException e) {
			   return null;
			}
	//	System.out.println(responseString);
		
	}
	public boolean checkSuccess(HttpURLConnection response, String expected_content) throws Exception, IOException{
		boolean test1=false;
		try{
			test1 =checkResponseCode(response);
			boolean test2=checkExpected_Content(response,expected_content);
			 
			
			return (test1 && test2);
		}catch(Exception e){
			return false;
		}	
	}
	private boolean checkExpected_Content(HttpURLConnection response,String expected_content) {
		if(expected_content!=null)
		  return response.toString().contains(expected_content);
		else
			return true;
	}
	private boolean checkResponseCode(HttpURLConnection response) throws Exception {
		boolean test1=false;
		int status = response.getResponseCode();
		if (status == HttpURLConnection.HTTP_OK
			||status == HttpURLConnection.HTTP_MOVED_TEMP
			|| status == HttpURLConnection.HTTP_MOVED_PERM
			|| status == HttpURLConnection.HTTP_SEE_OTHER)
			test1 = true;
		return test1;
			
	}
	public int editFailureNumbers(boolean result) throws Exception{
		
		if(result)
		{	
		  System.out.println("Ping is done successfully");
		  resetFailures();
		}
		else{
			PingService.failures++;
			shouldAlarm();
		}
		System.out.println("Failures are:"+PingService.failures);
		return PingService.failures;
	}
	
public boolean shouldAlarm() throws Exception {
		ReadConfig config = new ReadConfig();
		if(find(config.getAlert_conditions(),PingService.failures)){
			SendSMS sms=new	SendSMS();
			return true;
		}else
			return false;
		
	}
	

	public void resetFailures(){
		PingService.failures=0;
	}
	

	private boolean find(ArrayList alert_conditions, int failures) {
		boolean result=false;
		for(int i=0;i <alert_conditions.size();i++ ){
			if((Long)alert_conditions.get(i)==failures){
				 result = true;
				 break;
			}else 
				 result =false;
			
			
		}
			
		return result;
	}
	
	
	public void makePing(String URL, Double timeout, String Expected_string) throws Exception {
		HttpURLConnection response = GetRequest(URL,	timeout );
		boolean result = checkSuccess(response, Expected_string);
		editFailureNumbers(result);
		shouldAlarm();
		 System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@             @@@@@@@@@@@@@@@@@@@@@@@");
	}
	
	
}
